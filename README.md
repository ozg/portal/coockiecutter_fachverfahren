# Fachverfahren

Die Fachverfahren werden im Portalserver als Subprojekte verwaltet und
können unabhängig voneinander in ein Portal integriert werden.

Für ein einheitliches Ordner- und Dateilayout kann dieses
Cookiecutter-Template verwendet werden und es erleichtet auch die
Entwicklung eines neuen Fachverfahren.

# Voraussetzungen

- eine virtuelle Python-Umgebung ist vorhanden und aktiviert
- Cookiecutter ist installiert

  ```
  pip install cookiecutter
  ```

- Der Portal-Server ist installiert, siehe:

  https://gitlab.com/ozg/portal/server


# Neues Fachverfahren in den Portal-Server einbinden
In Ordner f11n (Kurzform von: Fachverfahren) können beliebige
Fachverfahren eingefügt werden, wenn sie der vorgegebenen
Ordnerstruktur folgen. 

```
cd ozg/f11n
cookiecutter https://gitlab.com/ozg/portal/coockiecutter_fachverfahren.git

```
# Wenn gitlab nicht funktioniert!

Falls der der direkte Zugriff auf das git-Repository nicht
funktioniert, kann das Template auch als zip-Datei heruntergeladen und
gespeichert werden. Der Aufruf sieht dann wie folgt aus:

```
cd ozg/f11n
cookiecutter /pfad/zum/download/der/zipdatei/coockiecutter_fachverfahren.zip

```

# Nacharbeiten

- einbinden des neuen Fachverfahren in den Portalserver, wie am Ende
  des Dialogs angezeigt
- Eventuell müssen die Dummyrouten für gleichnamige Fachverfahren
  angepasst und aus dem Portal-Server entfernt werden. z.B. hundesteuer,
  baumfaellung ... 
- eine Liste der vorhandenen Routen zeigt der Befehl

  ```
  proutes development.ini
  ```
  im Portal-Server an ...


