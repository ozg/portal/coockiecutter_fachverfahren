
def includeme(config):
    config.add_jinja2_search_path("ozg:templates")
    config.add_route('{{cookiecutter.package_name}}_1', '/f11n/{{cookiecutter.package_name}}/index')
    config.add_route('{{cookiecutter.package_name}}_2a', '/f11n/{{cookiecutter.package_name}}/form2a')
    config.add_route('{{cookiecutter.package_name}}_2b', '/f11n/{{cookiecutter.package_name}}/form2b')
    config.add_route('{{cookiecutter.package_name}}_3a', '/f11n/{{cookiecutter.package_name}}/form3a')
    config.add_route('{{cookiecutter.package_name}}_3b', '/f11n/{{cookiecutter.package_name}}/form3b')
    config.add_route('{{cookiecutter.package_name}}_4a', '/f11n/{{cookiecutter.package_name}}/form4a')
    config.add_route('{{cookiecutter.package_name}}_4b', '/f11n/{{cookiecutter.package_name}}/form4b')
    config.add_static_view('static_{{cookiecutter.package_name}}', 'static', cache_max_age=3600)    
    config.scan()
