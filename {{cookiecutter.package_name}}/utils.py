from pyramid.path import AssetResolver
resolver = AssetResolver()


def get_package_name():
    "Get package name"

    name = resolver.resolve('{{cookiecutter.package_name}}').pkg_name
    return name
