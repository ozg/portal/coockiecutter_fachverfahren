import pytest
from pydantic import BaseModel, ValidationError, validator

testdata = {'anrede': 'frau'}

class FormData(BaseModel):
    """ Werte, die wir erwarten

       Der Wert für das Feld farbe ist hier ein Pflichtfeld.
    """
    
    farbe: str

    @validator('farbe')
    def check_farbe(cls, v):
        if len(v) == 0:
            raise ValueError('Farbe ist ein Pflichtfeld!')
        return v.title()
    
def test_FormData_with_herr():
    fd = FormData(farbe='abc')
    assert fd.dict() == {'farbe':'Abc'}

def test_validator_farbe():
    with pytest.raises(ValidationError) as exc_info:
        FormData(farbe='')
    assert exc_info.value.errors() == [{'loc': ('farbe',),
                                        'msg': 'Farbe ist ein Pflichtfeld!',
                                        'type': 'value_error'}]

    
