import pytest
from pydantic import BaseModel, ValidationError, validator
import json

testdata = {'anrede': 'frau'}

class FormData(BaseModel):
    """ Werte, die wir erwarten

       Der Wert für das Feld farbe ist hier ein Pflichtfeld.
    """
    anrede = 'Frau'
    farbe: str
    speise: str = None
    checkbox_1: str = ''
    checkbox_2: str = 'FIM'
    checkbox_3: str = ''

    @validator('farbe')
    def check_farbe(cls, v):
        if len(v) == 0:
            raise ValueError('Farbe ist ein Pflichtfeld!')
        return v.title()
    
def test_FormData_with_herr():
    fd = FormData(farbe='abc')
    assert fd.dict() == {'anrede': 'Frau',
                         'checkbox_1': '',
                         'checkbox_2': 'FIM',
                         'checkbox_3': '',
                         'speise': None,
                         'farbe': 'Abc'}

def test_validator_farbe():
    with pytest.raises(ValidationError) as exc_info:
        FormData(farbe='')
    assert exc_info.value.errors() == [{'loc': ('farbe',),
                                        'msg': 'Farbe ist ein Pflichtfeld!',
                                        'type': 'value_error'}]

    
def test_with_form_input():

    FormData.parse_obj({'anrede': 'Frau',
                  'checkbox_1': 'OZG',
                  'checkbox_2': '',
                  'checkbox_3': '',
                  'speise': None,
                  'farbe': 'rot'})
    try:
        # Farbe fehlt! also Fehler!
        FormData(anrede='Frau',
                 checkbox_1='OZG',
                 checkbox_2='',
                 checkbox_3='',
                 speise=None,
                 farbe='')
    except ValidationError as e:
        fehler = json.loads(e.json())
    assert fehler == [{'loc': ['farbe'], 'msg': 'Farbe ist ein Pflichtfeld!', 'type': 'value_error'}]
    
