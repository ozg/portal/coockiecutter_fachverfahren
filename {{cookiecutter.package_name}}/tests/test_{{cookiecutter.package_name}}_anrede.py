import pytest
from pydantic import BaseModel, ValidationError, validator

testdata = {'anrede': 'frau'}

class FormData(BaseModel):
    """ Werte, die wir erwarten"""
    
    anrede = 'herr'

    @validator('anrede')
    def check_anrede(cls, v):
        if v not in ['frau', 'herr']:
            raise ValueError('Nur die Anrede Herr und Frau sind zulaessig!')
        return v.title()
    
def test_FormData():
    fd = FormData()
    assert fd.dict()['anrede'] == 'herr'

def test_FormData_with_herr():
    fd = FormData(anrede='herr')
    assert fd.dict() == {'anrede':'Herr'}

def test_FormData_with_frau():
    fd = FormData(anrede='frau')
    assert fd.dict() == {'anrede':'Frau'}
    
def test_validator_anrede():
    with pytest.raises(ValidationError) as exc_info:
        FormData(anrede='herrlich')
    assert exc_info.value.errors() == [{'loc': ('anrede',),
                                        'msg': 'Nur die Anrede Herr und Frau sind zulaessig!',
                                        'type': 'value_error'}]
