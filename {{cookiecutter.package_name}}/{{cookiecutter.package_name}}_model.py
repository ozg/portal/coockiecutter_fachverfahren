from pydantic import BaseModel, ValidationError, validator
import json

class FormData(BaseModel):
    """ Werte, die wir erwarten

       Der Wert für das Feld farbe ist hier ein Pflichtfeld.
    """
    anrede = 'Frau'
    farbe: str
    speise: str = None
    checkbox_1: str = ''
    checkbox_2: str = ''
    checkbox_3: str = ''

    @validator('farbe')
    def check_farbe(cls, v):
        if len(v) == 0:
            raise ValueError('Farbe ist ein Pflichtfeld!')
        return v.title()

def check_form(data):
    fehler = []
    try:
        FormData(**data)
    except ValidationError as e:
        fehler = json.loads(e.json())

    return fehler

if __name__ == '__main__':

    data = {'anrede': 'Frau',
            'checkbox_1':'OZG',
            'checkbox_2': '',
            'checkbox_3': '',
            'speise': None,
            'farbe':''}
    fehler = check_form(data)
    print(fehler)
    for k in data.keys():
        print("key: ", k, " value: ", data[k])
        
