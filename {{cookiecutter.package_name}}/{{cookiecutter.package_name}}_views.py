from pyramid.view import view_config
from .utils import get_package_name
from pyramid.renderers import render
from pyramid.response import Response
import json

from .{{cookiecutter.package_name}}_model import check_form

@view_config(route_name='{{cookiecutter.package_name}}_1',
             renderer='./templates/{{cookiecutter.package_name}}.jinja2')
def {{cookiecutter.package_name}}_demo(request):
    return {'topic': '{{cookiecutter.package_name}}'}


@view_config(route_name='{{cookiecutter.package_name}}_2a',
             renderer='./templates/{{cookiecutter.package_name}}_form2a.jinja2')
def {{cookiecutter.package_name}}_2a(request):
    """ Liefert ein Formuala aus

    Die Verarbeitung der Formulardaten erfolgt
    in der View: form2b
    """
    
    return {'topic': '{{cookiecutter.package_name}}: Daten als Dictionary verarbeiten',

            'action': 'form2b'}

@view_config(route_name='{{cookiecutter.package_name}}_2b')
def {{cookiecutter.package_name}}_2b(request):
    """
    Hier nicht genutzt:

    data.update(request.GET)
    data.update(request.matchdict)
    """

    data = {}
    data.update(request.POST)
    fehler = check_form(data)
    result = render('./templates/{{cookiecutter.package_name}}_form2b.jinja2',
                    {'data': data,
                     'fehler': fehler}, 
                     request=request)
    
    return Response(result)

@view_config(route_name='{{cookiecutter.package_name}}_3a',
             renderer='./templates/{{cookiecutter.package_name}}_form2a.jinja2')
def {{cookiecutter.package_name}}_3a(request):
    """ Liefert ein Formualar aus

    Die Verarbeitung der Formulardaten erfolgt
    in der View: form3b
    """
    return {'topic': 'Weitere Formularvarianten',
            'action': 'form3b'}


@view_config(route_name='{{cookiecutter.package_name}}_3b',
             renderer="json")
def {{cookiecutter.package_name}}_3b(request):
    """Ausgabe der Daten im JSON-Format. """

    data = {}
    data.update(request.POST)

    return data


@view_config(route_name='{{cookiecutter.package_name}}_4a',
             renderer='./templates/{{cookiecutter.package_name}}_form4a.jinja2')
def {{cookiecutter.package_name}}_4a(request):
    """ Liefert ein Formualar aus

    Die Verarbeitung der Formulardaten erfolgt
    in der View: form2b
    """
    return {'topic': 'Weitere Formularvarianten',
            'action': 'form4b'}


@view_config(route_name='{{cookiecutter.package_name}}_4b')
def {{cookiecutter.package_name}}_4b(request):
    """Ausgabe der Daten..."""

    data = {}
    data.update(request.POST)
    fehler = check_form(data)
    result = render('./templates/{{cookiecutter.package_name}}_form2b.jinja2',
                    {'data': data,
                     'fehler': fehler}, 
                     request=request)
    
    return Response(result)

