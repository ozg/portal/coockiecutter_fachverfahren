#!/usr/bin/env python
# -*- coding: utf-8 -*-

print("""

Konfigurations-Hinweise
=======================

1. Fügen Sie in der  main-Funktion in __init__.py
   des Portals die folgende Zeile ein:

   config.include('.f11n.{{cookiecutter.package_name}}')

2. Im Menü des Portalservers (layout.jinja2) fügen Sie an 
   geeigneter Stelle das folgende Menü hinzu:

	<li class="menu-item"
	    tabindex="1">
          <a href="/f11n/{{cookiecutter.package_name}}/index">{{cookiecutter.package_name}}</a>
        </li>
""")
